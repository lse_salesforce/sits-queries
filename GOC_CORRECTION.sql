/*usp SRS_GOC UK telephone Update.sql

Stored Procedure to Consolidate email addresses in SRS_GOC to:
1) Remove duplicate values
2) Populate the 10 email address columns starting with goc_em01 to goc_em10 such that the earlier fields are populated first.
3) Takes INPUT parameters for p_academic_year (e.g. 2018/9) and p_census_period (e.g. Sep)

Version Date      Auth  Comment
0.1     25/04/19  LNT   Initial
0.2     28/06/19  LNT   Re-written without CTEs for SITS
0.3     10/07/19  LNT   Conversion to Stored Procedure with parameters: p_academic_year, p_census_period
0.4     10/07/19  LNT   Added substr() function to limit the length of values generated for the update statements to the column widths 
*/

accept cdb prompt 'Enter CDB : '

alter session set container=&&CDB;

set veri off

grant select on intuit.srs_goc to taoa; 
grant select,update on intuit.srs_goc to intuit_lse;

create or replace procedure intuit_lse.goc_correction (p_academic_year IN varchar2, p_census_period IN varchar2)
AS

BEGIN

UPDATE intuit.srs_goc goc
SET (goc_um01,
     goc_um02,
     goc_um03,
     goc_um04,
     goc_um05,
     goc_um06,
     goc_um07,
     goc_um08,
     goc_um09,
     goc_um10,
     goc_em01,
     goc_em02,
     goc_em03,
     goc_em04,
     goc_em05,
     goc_em06,
     goc_em07,
     goc_em08,
     goc_em09,
     goc_em10,
     goc_it01,
     goc_it02,
     goc_it03,
     goc_it04,
     goc_it05,
     goc_it06,
     goc_it07,
     goc_it08,
     goc_it09,
     goc_it10,
     goc_ut01,
     goc_ut02,
     goc_ut03,
     goc_ut04,
     goc_ut05,
     goc_ut06,
     goc_ut07,
     goc_ut08,
     goc_ut09,
     goc_ut10
) = (
SELECT --goc.goc_insc,
       --goc.goc_rcid,
       --goc.goc_gops,
/*Mobile Telephone 11*/
       SUBSTR(goc_um.goc_um01_u, CASE WHEN LENGTH(goc_um.goc_um01_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um01_u) * -1 END) goc_um01_u,
       SUBSTR(goc_um.goc_um02_u, CASE WHEN LENGTH(goc_um.goc_um02_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um02_u) * -1 END) goc_um02_u,
       SUBSTR(goc_um.goc_um03_u, CASE WHEN LENGTH(goc_um.goc_um03_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um03_u) * -1 END) goc_um03_u,
       SUBSTR(goc_um.goc_um04_u, CASE WHEN LENGTH(goc_um.goc_um04_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um04_u) * -1 END) goc_um04_u,
       SUBSTR(goc_um.goc_um05_u, CASE WHEN LENGTH(goc_um.goc_um05_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um05_u) * -1 END) goc_um05_u,
       SUBSTR(goc_um.goc_um06_u, CASE WHEN LENGTH(goc_um.goc_um06_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um06_u) * -1 END) goc_um06_u,
       SUBSTR(goc_um.goc_um07_u, CASE WHEN LENGTH(goc_um.goc_um07_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um07_u) * -1 END) goc_um07_u,
       SUBSTR(goc_um.goc_um08_u, CASE WHEN LENGTH(goc_um.goc_um08_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um08_u) * -1 END) goc_um08_u,
       SUBSTR(goc_um.goc_um09_u, CASE WHEN LENGTH(goc_um.goc_um09_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um09_u) * -1 END) goc_um09_u,
       SUBSTR(goc_um.goc_um10_u, CASE WHEN LENGTH(goc_um.goc_um10_u) > 11 THEN - 11 ELSE LENGTH(goc_um.goc_um10_u) * -1 END) goc_um10_u,
/*Email 255 chars*/
       SUBSTR(goc_em.goc_em01_u, CASE WHEN LENGTH(goc_em.goc_em01_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em01_u) * -1 END) goc_em01_u,
       SUBSTR(goc_em.goc_em02_u, CASE WHEN LENGTH(goc_em.goc_em02_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em02_u) * -1 END) goc_em02_u,
       SUBSTR(goc_em.goc_em03_u, CASE WHEN LENGTH(goc_em.goc_em03_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em03_u) * -1 END) goc_em03_u,
       SUBSTR(goc_em.goc_em04_u, CASE WHEN LENGTH(goc_em.goc_em04_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em04_u) * -1 END) goc_em04_u,
       SUBSTR(goc_em.goc_em05_u, CASE WHEN LENGTH(goc_em.goc_em05_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em05_u) * -1 END) goc_em05_u,
       SUBSTR(goc_em.goc_em06_u, CASE WHEN LENGTH(goc_em.goc_em06_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em06_u) * -1 END) goc_em06_u,
       SUBSTR(goc_em.goc_em07_u, CASE WHEN LENGTH(goc_em.goc_em07_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em07_u) * -1 END) goc_em07_u,
       SUBSTR(goc_em.goc_em08_u, CASE WHEN LENGTH(goc_em.goc_em08_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em08_u) * -1 END) goc_em08_u,
       SUBSTR(goc_em.goc_em09_u, CASE WHEN LENGTH(goc_em.goc_em09_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em09_u) * -1 END) goc_em09_u,
       SUBSTR(goc_em.goc_em10_u, CASE WHEN LENGTH(goc_em.goc_em10_u) > 255 THEN - 255 ELSE LENGTH(goc_em.goc_em10_u) * -1 END) goc_em10_u,
/*International Telephone 17 chars*/
       SUBSTR(goc_it.goc_it01_u, CASE WHEN LENGTH(goc_it.goc_it01_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it01_u) * -1 END) goc_it01_u,       
       SUBSTR(goc_it.goc_it02_u, CASE WHEN LENGTH(goc_it.goc_it02_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it02_u) * -1 END) goc_it02_u,
       SUBSTR(goc_it.goc_it03_u, CASE WHEN LENGTH(goc_it.goc_it03_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it03_u) * -1 END) goc_it03_u,
       SUBSTR(goc_it.goc_it04_u, CASE WHEN LENGTH(goc_it.goc_it04_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it04_u) * -1 END) goc_it04_u,
       SUBSTR(goc_it.goc_it05_u, CASE WHEN LENGTH(goc_it.goc_it05_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it05_u) * -1 END) goc_it05_u,
       SUBSTR(goc_it.goc_it06_u, CASE WHEN LENGTH(goc_it.goc_it06_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it06_u) * -1 END) goc_it06_u,
       SUBSTR(goc_it.goc_it07_u, CASE WHEN LENGTH(goc_it.goc_it07_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it07_u) * -1 END) goc_it07_u,
       SUBSTR(goc_it.goc_it08_u, CASE WHEN LENGTH(goc_it.goc_it08_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it08_u) * -1 END) goc_it08_u,
       SUBSTR(goc_it.goc_it09_u, CASE WHEN LENGTH(goc_it.goc_it09_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it09_u) * -1 END) goc_it09_u,
       SUBSTR(goc_it.goc_it10_u, CASE WHEN LENGTH(goc_it.goc_it10_u) > 17 THEN - 17 ELSE LENGTH(goc_it.goc_it10_u) * -1 END) goc_it10_u,
/*UK telephone 11 chars*/
       SUBSTR(goc_ut.goc_ut01_u, CASE WHEN LENGTH(goc_ut.goc_ut01_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut01_u) * -1 END) goc_ut01_u,
       SUBSTR(goc_ut.goc_ut02_u, CASE WHEN LENGTH(goc_ut.goc_ut02_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut02_u) * -1 END) goc_ut02_u,
       SUBSTR(goc_ut.goc_ut03_u, CASE WHEN LENGTH(goc_ut.goc_ut03_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut03_u) * -1 END) goc_ut03_u,
       SUBSTR(goc_ut.goc_ut04_u, CASE WHEN LENGTH(goc_ut.goc_ut04_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut04_u) * -1 END) goc_ut04_u,
       SUBSTR(goc_ut.goc_ut05_u, CASE WHEN LENGTH(goc_ut.goc_ut05_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut05_u) * -1 END) goc_ut05_u,
       SUBSTR(goc_ut.goc_ut06_u, CASE WHEN LENGTH(goc_ut.goc_ut06_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut06_u) * -1 END) goc_ut06_u,
       SUBSTR(goc_ut.goc_ut07_u, CASE WHEN LENGTH(goc_ut.goc_ut07_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut07_u) * -1 END) goc_ut07_u,
       SUBSTR(goc_ut.goc_ut08_u, CASE WHEN LENGTH(goc_ut.goc_ut08_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut08_u) * -1 END) goc_ut08_u,
       SUBSTR(goc_ut.goc_ut09_u, CASE WHEN LENGTH(goc_ut.goc_ut09_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut09_u) * -1 END) goc_ut09_u,
       SUBSTR(goc_ut.goc_ut10_u, CASE WHEN LENGTH(goc_ut.goc_ut10_u) > 11 THEN - 11 ELSE LENGTH(goc_ut.goc_ut10_u) * -1 END) goc_ut10_u
FROM   intuit.srs_goc goc_u LEFT OUTER JOIN (SELECT goc_insc,
        goc_rcid,
        goc_gops,
        "'1'"  goc_um01_u,
        "'2'"  goc_um02_u,
        "'3'"  goc_um03_u,
        "'4'"  goc_um04_u,
        "'5'"  goc_um05_u,
        "'6'"  goc_um06_u,
        "'7'"  goc_um07_u,
        "'8'"  goc_um08_u,
        "'9'"  goc_um09_u,
        "'10'" goc_um10_u       
 FROM
(SELECT g.goc_insc,
        g.goc_rcid,
        g.goc_gops,
        g.rn,
        g.goc_um01
 FROM   (
SELECT ged.*,
       row_number() OVER (PARTITION BY goc_insc, goc_rcid, goc_gops ORDER BY goc_insc, goc_rcid, goc_gops) rn
FROM   (
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um01
FROM    intuit.srs_goc
WHERE   goc_um01 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um02
FROM    intuit.srs_goc
WHERE   goc_um02 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um03
FROM    intuit.srs_goc
WHERE   goc_um03 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um04
FROM    intuit.srs_goc
WHERE   goc_um04 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um05
FROM    intuit.srs_goc
WHERE   goc_um05 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um06
FROM    intuit.srs_goc
WHERE   goc_um06 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um07
FROM    intuit.srs_goc
WHERE   goc_um07 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um08
FROM    intuit.srs_goc
WHERE   goc_um08 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um09
FROM    intuit.srs_goc
WHERE   goc_um09 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_um10
FROM    intuit.srs_goc
WHERE   goc_um10 IS NOT NULL
) ged
) g)
 PIVOT
 (MIN(goc_um01) FOR rn IN ('1','2','3','4','5','6','7','8','9','10'))
) goc_um    ON (goc_u.goc_insc = goc_um.goc_insc AND goc_u.goc_rcid = goc_um.goc_rcid AND goc_u.goc_gops = goc_um.goc_gops)
                            LEFT OUTER JOIN (SELECT goc_insc,
        goc_rcid,
        goc_gops,
        "'1'"  goc_em01_u,
        "'2'"  goc_em02_u,
        "'3'"  goc_em03_u,
        "'4'"  goc_em04_u,
        "'5'"  goc_em05_u,
        "'6'"  goc_em06_u,
        "'7'"  goc_em07_u,
        "'8'"  goc_em08_u,
        "'9'"  goc_em09_u,
        "'10'" goc_em10_u       
 FROM
(SELECT g.goc_insc,
        g.goc_rcid,
        g.goc_gops,
        g.rn,
        g.goc_em01
 FROM   (
SELECT ged.*,
       row_number() OVER (PARTITION BY goc_insc, goc_rcid, goc_gops ORDER BY goc_insc, goc_rcid, goc_gops) rn
FROM   (
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em01
FROM    intuit.srs_goc
WHERE   goc_em01 IS NOT NULL
AND     goc_em01 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em02
FROM    intuit.srs_goc
WHERE   goc_em02 IS NOT NULL
AND     goc_em02 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em03
FROM    intuit.srs_goc
WHERE   goc_em03 IS NOT NULL
AND     goc_em03 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em04
FROM    intuit.srs_goc
WHERE   goc_em04 IS NOT NULL
AND     goc_em04 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em05
FROM    intuit.srs_goc
WHERE   goc_em05 IS NOT NULL
AND     goc_em05 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em06
FROM    intuit.srs_goc
WHERE   goc_em06 IS NOT NULL
AND     goc_em06 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em07
FROM    intuit.srs_goc
WHERE   goc_em07 IS NOT NULL
AND     goc_em07 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em08
FROM    intuit.srs_goc
WHERE   goc_em08 IS NOT NULL
AND     goc_em08 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em09
FROM    intuit.srs_goc
WHERE   goc_em09 IS NOT NULL
AND     goc_em09 NOT LIKE '%@lse.ac.uk'
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_em10
FROM    intuit.srs_goc
WHERE   goc_em10 IS NOT NULL
AND     goc_em10 NOT LIKE '%@lse.ac.uk'
) ged
) g)
 PIVOT
 (MIN(goc_em01) FOR rn IN ('1','2','3','4','5','6','7','8','9','10'))
) goc_em ON (goc_u.goc_insc = goc_em.goc_insc AND goc_u.goc_rcid = goc_em.goc_rcid AND goc_u.goc_gops = goc_em.goc_gops)
                            LEFT OUTER JOIN (SELECT goc_insc,
        goc_rcid,
        goc_gops,
        "'1'"  goc_it01_u,
        "'2'"  goc_it02_u,
        "'3'"  goc_it03_u,
        "'4'"  goc_it04_u,
        "'5'"  goc_it05_u,
        "'6'"  goc_it06_u,
        "'7'"  goc_it07_u,
        "'8'"  goc_it08_u,
        "'9'"  goc_it09_u,
        "'10'" goc_it10_u       
 FROM
(SELECT g.goc_insc,
        g.goc_rcid,
        g.goc_gops,
        g.rn,
        g.goc_it01
 FROM   (
SELECT ged.*,
       row_number() OVER (PARTITION BY goc_insc, goc_rcid, goc_gops ORDER BY goc_insc, goc_rcid, goc_gops) rn
FROM   (
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it01 LIKE '00%' THEN goc_it01
             WHEN goc_it01 LIKE '0%' THEN '0' || goc_it01
             ELSE '00' || goc_it01
        END goc_it01
FROM    intuit.srs_goc
WHERE   goc_it01 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it02 LIKE '00%' THEN goc_it02
             WHEN goc_it02 LIKE '0%' THEN '0' || goc_it02
             ELSE '00' || goc_it02
        END goc_it02
FROM    intuit.srs_goc
WHERE   goc_it02 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it03 LIKE '00%' THEN goc_it03
             WHEN goc_it03 LIKE '0%' THEN '0' || goc_it03
             ELSE '00' || goc_it03
        END goc_it03
FROM    intuit.srs_goc
WHERE   goc_it03 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it04 LIKE '00%' THEN goc_it04
             WHEN goc_it04 LIKE '0%' THEN '0' || goc_it04
             ELSE '00' || goc_it04
        END goc_it04
FROM    intuit.srs_goc
WHERE   goc_it04 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it05 LIKE '00%' THEN goc_it05
             WHEN goc_it05 LIKE '0%' THEN '0' || goc_it05
             ELSE '00' || goc_it05
        END goc_it05
FROM    intuit.srs_goc
WHERE   goc_it05 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it06 LIKE '00%' THEN goc_it06
             WHEN goc_it06 LIKE '0%' THEN '0' || goc_it06
             ELSE '00' || goc_it06
        END goc_it06
FROM    intuit.srs_goc
WHERE   goc_it06 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it07 LIKE '00%' THEN goc_it07
             WHEN goc_it07 LIKE '0%' THEN '0' || goc_it07
             ELSE '00' || goc_it07
        END goc_it07
FROM    intuit.srs_goc
WHERE   goc_it07 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it08 LIKE '00%' THEN goc_it08
             WHEN goc_it08 LIKE '0%' THEN '0' || goc_it08
             ELSE '00' || goc_it08
        END goc_it08
FROM    intuit.srs_goc
WHERE   goc_it08 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it09 LIKE '00%' THEN goc_it09
             WHEN goc_it09 LIKE '0%' THEN '0' || goc_it09
             ELSE '00' || goc_it09
        END goc_it09
FROM    intuit.srs_goc
WHERE   goc_it09 IS NOT NULL
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        CASE WHEN goc_it10 LIKE '00%' THEN goc_it10
             WHEN goc_it10 LIKE '0%' THEN '0' || goc_it10
             ELSE '00' || goc_it10
        END goc_it10
FROM    intuit.srs_goc
WHERE   goc_it10 IS NOT NULL
) ged
) g)
 PIVOT
 (MIN(goc_it01) FOR rn IN ('1','2','3','4','5','6','7','8','9','10'))
) goc_it    ON (goc_u.goc_insc = goc_it.goc_insc AND goc_u.goc_rcid = goc_it.goc_rcid AND goc_u.goc_gops = goc_it.goc_gops)
                            LEFT OUTER JOIN (SELECT goc_insc,
        goc_rcid,
        goc_gops,
        "'1'"  goc_ut01_u,
        "'2'"  goc_ut02_u,
        "'3'"  goc_ut03_u,
        "'4'"  goc_ut04_u,
        "'5'"  goc_ut05_u,
        "'6'"  goc_ut06_u,
        "'7'"  goc_ut07_u,
        "'8'"  goc_ut08_u,
        "'9'"  goc_ut09_u,
        "'10'" goc_ut10_u       
 FROM
(SELECT g.goc_insc,
        g.goc_rcid,
        g.goc_gops,
        g.rn,
        g.goc_ut01
 FROM   (
SELECT ged.*,
       row_number() OVER (PARTITION BY goc_insc, goc_rcid, goc_gops ORDER BY goc_insc, goc_rcid, goc_gops) rn
FROM   (
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut01
FROM    intuit.srs_goc
WHERE   goc_ut01 IS NOT NULL
AND     goc_ut01 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut02
FROM    intuit.srs_goc
WHERE   goc_ut02 IS NOT NULL
AND     goc_ut02 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut03
FROM    intuit.srs_goc
WHERE   goc_ut03 IS NOT NULL
AND     goc_ut03 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut04
FROM    intuit.srs_goc
WHERE   goc_ut04 IS NOT NULL
AND     goc_ut04 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut05
FROM    intuit.srs_goc
WHERE   goc_ut05 IS NOT NULL
AND     goc_ut05 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut06
FROM    intuit.srs_goc
WHERE   goc_ut06 IS NOT NULL
AND     goc_ut06 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut07
FROM    intuit.srs_goc
WHERE   goc_ut07 IS NOT NULL
AND     goc_ut07 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut08
FROM    intuit.srs_goc
WHERE   goc_ut08 IS NOT NULL
AND     goc_ut08 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut09
FROM    intuit.srs_goc
WHERE   goc_ut09 IS NOT NULL
AND     goc_ut09 NOT IN ('02074057686','02074031945','02074924960')
UNION 
SELECT  goc_insc,
        goc_rcid,
        goc_gops, 
        goc_ut10
FROM    intuit.srs_goc
WHERE   goc_ut10 IS NOT NULL
AND     goc_ut10 NOT IN ('02074057686','02074031945','02074924960')
) ged
) g)
 PIVOT
 (MIN(goc_ut01) FOR rn IN ('1','2','3','4','5','6','7','8','9','10'))
) goc_ut    ON (goc_u.goc_insc = goc_ut.goc_insc AND goc_u.goc_rcid = goc_ut.goc_rcid AND goc_u.goc_gops = goc_ut.goc_gops)

WHERE  goc.goc_insc = goc_u.goc_insc
AND    goc.goc_rcid = goc_u.goc_rcid
AND    goc.goc_gops = goc_u.goc_gops

/*Filter GOC records by input parameters*/
AND    goc_u.goc_ayrc = p_academic_year
AND    goc_u.goc_cenp = p_census_period
);
END;
/

exec intuit_lse.goc_correction('2017/8','Sep');    